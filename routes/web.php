<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('a-propos-de-nous', [HomeController::class, 'apropos'])->name('apropos');
Route::get('contact', [HomeController::class, 'contact'])->name('contact');
Route::get('services', [HomeController::class, 'services'])->name('services');


Route::resource('categories', CategoryController::class)
                                    ->names('categories');

Route::resource('posts', PostController::class)->names("posts");