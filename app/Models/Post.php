<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'image',
        'date_pub',
        'author',
        'status',
        'category_id'
    ];

    /**
     * Cette fonction permet de récupérer
     * la catégory d'un article
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
