@extends("layouts.app")

@section("title", "Modification d'une catégorie")

@section("content")
    <div class="row">
        <h3>Modification d'une catégorie</h3>
        <form 
        action="{{ route("categories.update", $category->id) }}" 
        method="post">
        
            @csrf
            @method("PUT")

            @include("categories.form")
            
            <div class="d-grid gap-2">
                <button type="submit" class="btn btn-warning">
                    Enregistrer la catégorie
                </button>
            </div>
        </form>
    </div>
@endsection